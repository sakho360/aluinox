<?php

namespace AppBundle\Handler;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

/**
 * Created by PhpStorm.
 * User: qualshore
 * Date: 14/11/16
 * Time: 17:27
 */
class AuthenticationHandler implements AuthenticationSuccessHandlerInterface
{

    protected $router;
    protected $security;
    protected $tokenStorage;
    protected $container;


    public function __construct(Container $container, RouterInterface $router, AuthorizationCheckerInterface $security, TokenStorage $tokenStorage)
    {
        $this->router = $router;
        $this->security = $security;
        $this->tokenStorage = $tokenStorage;
        $this->container = $container;
    }

    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request $request
     * @param TokenInterface $token
     *
     * @return Response never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $key = "_security_main.target_path";
        $session = $request->getSession();

        // Get list of roles for current user
        $roles = $token->getRoles();
        // Tranform this list in array
        $rolesTab = array_map(function ($role) {
            return $role->getRole();
        }, $roles);
        // If is a admin or super admin we redirect to the backoffice area
        if (in_array('ROLE_ADMIN', $rolesTab, true) || in_array('ROLE_SUPER_ADMIN', $rolesTab, true)){
            return new RedirectResponse($this->router->generate('chantier_index'));
        }

//        $captcha = $request->request->get('g-recaptcha-response');
//        if(!$captcha){
//            throw new BadCredentialsException('Le captcha n\'est pas valdie');
//        }


        if($session->has($key)){
            $url = $session->get($key);
        }else{
            $url = $this->router->generate('chantier_index');
        }

        $user = $this->getUser();
        $_this = $this->container;

//        $factory = $_this->get('sm.factory');
//        $userSm = $factory->get($user, 'simple');
//
//        if($userSm->getState()=="new"){
//            $userSm->apply('ask_number', true);
//        }
        //print_r($userSm->getPossibleTransitions());die;

        $response = new RedirectResponse($url);


        return $response;
    }

    public function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()->getUser()) {
            return null;
        }

        if (!is_object($user = $token)) {
            return null;
        }

        return $user;
    }
}