<?php
/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 2/13/18
 * Time: 1:56 PM
 */
namespace AppBundle\EventListener;

use AppBundle\Events\ChantierEvent;
use AppBundle\Events\DepenseEvent;
use AppBundle\Events\NotificationEvent;
use AppBundle\Model\NotificationObject;
use AppBundle\Notification\AbstractNotification;
use AppBundle\Notification\NotificationManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DepenseEventListner implements EventSubscriberInterface{

    private $notifier;
    private $em;

    public function __construct (AbstractNotification $notification , EntityManagerInterface $manager)
    {
        $this->notifier = $notification;
        $this->em = $manager;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents ()
    {
        // TODO: Implement getSubscribedEvents() method.
       return array(
           NotificationEvent::NEW_DEPENSE => 'onNewDepense',
           NotificationEvent::NEW_CHANTIER => 'onNewChantier',
       );
    }

    public function onNewDepense (DepenseEvent $event)
    {
        $depense  = $event->getDepense();
        $user = $depense->getChantier()->getUser();
        $subject = 'Création de dépense';
        $template = 'Emails/new_depense_email.html.twig';
        $params = array('user' => $user, 'depense' =>$depense);

        $notification = new NotificationObject(NotificationManager::EMAIL_ONLY, $user->getEmail(),$template,$params,$subject);
        $this->notifier->notify($notification);

    }

    public function onNewChantier (ChantierEvent $event)
    {
        $chantier = $event->getChantier();
        $user = $chantier->getUser();
        $subject = 'Creation de chantier';
        $template = 'Emails/new_chantier_email.html.twig';
        $params = array('user' => $user, 'chantier' =>$chantier);

        $notification = new NotificationObject(NotificationManager::EMAIL_ONLY, $user->getEmail(),$template,$params,$subject);
        $this->notifier->notify($notification);
    }
}