<?php
/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 11/23/17
 * Time: 12:25 PM
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\Chantier;
use AppBundle\Events\ChantierEvent;
use AppBundle\Events\NotificationEvent;
use AppBundle\Model\NotificationObject;
use AppBundle\Notification\AbstractNotification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ActiveChantierEventListener implements EventSubscriberInterface
{

    private $em;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->em = $manager;
    }
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
        return array(
            NotificationEvent::ACTIVE_CHANTIER => 'onActiveChantier',
            //NotificationEvent::NEW_CHANTIER => 'onNewChantier',
        );
    }

    public function onActiveChantier(ChantierEvent $event)
    {
        $chantier = $event->getChantier();
        $chantier->setActive(false);
        $this->em->persist($chantier);
        $this->em->flush();
    }

/*    public function onNewChantier (ChantierEvent $event)
    {

        $chantier = $event->getChantier();
        $user = $chantier->getUser();

        $subject = 'Création de chantier';
        $template = 'Emails/new_chantier_email.html.twig';
        $params = array('user' => $user, 'chantier' =>$chantier);

        $notification = new NotificationObject(NotificationManager::EMAIL_ONLY, $user->getEmail(),$template,$params,$subject);
        $this->notifier->notify($notification);

    }*/

}