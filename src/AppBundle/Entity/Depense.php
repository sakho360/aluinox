<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Depense
 *
 * @ORM\Table(name="depense")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DepenseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Depense
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="string", length=255)
     */
    private $nature;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime")
     */
    private $createAt;

    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="decimal", precision=16, scale=2)
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(name="beneficiaire", type="string", length=255)
     */
    private $beneficiaire;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;



    /**
     * @return Chantier
     */
    public function getChantier()
    {
        return $this->chantier;
    }

    /**
     * @param Chantier $chantier
     */
    public function setChantier($chantier)
    {
        $this->chantier = $chantier;
    }

    /**
     * @ORM\Column(type="string")
     */
    private $typeDepense;

    /**
     * @return mixed
     */
    public function getTypeDepense()
    {
        return $this->typeDepense;
    }

    /**
     * @param mixed $typeDepense
     */
    public function setTypeDepense($typeDepense)
    {
        $this->typeDepense = $typeDepense;
    }

    /**
     * @var Chantier
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Chantier")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $chantier;

    public function __construct()
    {
        $this->createAt = new \DateTime();
        $this->typeDepense = 'Font du chantier';
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nature
     *
     * @param string $nature
     *
     * @return Depense
     */
    public function setNature($nature)
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * Get nature
     *
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Depense
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param string $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }




    /**
     * Set beneficiaire
     *
     * @param string $beneficiaire
     *
     * @return Depense
     */
    public function setBeneficiaire($beneficiaire)
    {
        $this->beneficiaire = $beneficiaire;

        return $this;
    }

    /**
     * Get beneficiaire
     *
     * @return string
     */
    public function getBeneficiaire()
    {
        return $this->beneficiaire;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Depense
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
