<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Services
 *
 * @ORM\Table(name="services")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServicesRepository")
 */
class Services
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nature_ceintrage", type="string", length=255)
     */
    private $natureCeintrage;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=255)
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\Column(name="telephone", type="integer")
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="decimal", precision=10, scale=2)
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_avance", type="decimal", precision=10, scale=2)
     */
    private $montantAvance;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_restant", type="decimal", precision=10, scale=2)
     */
    private $montantRestant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="date")
     */
    private $dateCreation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set natureCeintrage
     *
     * @param string $natureCeintrage
     *
     * @return Services
     */
    public function setNatureCeintrage($natureCeintrage)
    {
        $this->natureCeintrage = $natureCeintrage;

        return $this;
    }

    /**
     * Get natureCeintrage
     *
     * @return string
     */
    public function getNatureCeintrage()
    {
        return $this->natureCeintrage;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return Services
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set telephone
     *
     * @param integer $telephone
     *
     * @return Services
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return int
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return Services
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set montantAvance
     *
     * @param string $montantAvance
     *
     * @return Services
     */
    public function setMontantAvance($montantAvance)
    {
        $this->montantAvance = $montantAvance;

        return $this;
    }

    /**
     * Get montantAvance
     *
     * @return string
     */
    public function getMontantAvance()
    {
        return $this->montantAvance;
    }

    /**
     * Set montantRestant
     *
     * @param string $montantRestant
     *
     * @return Services
     */
    public function setMontantRestant($montantRestant)
    {
        $this->montantRestant = $montantRestant;

        return $this;
    }

    /**
     * Get montantRestant
     *
     * @return string
     */
    public function getMontantRestant()
    {
        $rest = $this->montantRestant = $this->getMontant() - $this->getMontantAvance();
        if ($rest >= 0){
            return $this->montantRestant = $this->getMontant() - $this->getMontantAvance();
        }
        else
            return 'invalid';
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Services
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function __construct ()
    {
        $this->dateCreation = new \DateTime();
    }


}
