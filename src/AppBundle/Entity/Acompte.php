<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Validation as Assert;

/**
 * Acompte
 *
 * @ORM\Table(name="acompte")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AcompteRepository")
 */
class Acompte
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="string", length=255)
     */
    private $nature;

    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="decimal", precision=10, scale=2)
     */
    private $montant;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_acompt", type="datetime")
     */
    private $dateAcompt;


    /**
     * @var Chantier
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Chantier")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $chantier;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nature
     *
     * @param string $nature
     *
     * @return Acompte
     */
    public function setNature($nature)
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * Get nature
     *
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return Acompte
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set dateAcompt
     *
     * @param \DateTime $dateAcompt
     *
     * @return Acompte
     */
    public function setDateAcompt($dateAcompt)
    {
        $this->dateAcompt = $dateAcompt;

        return $this;
    }

    /**
     * Get dateAcompt
     *
     * @return \DateTime
     */
    public function getDateAcompt()
    {
        return $this->dateAcompt;
    }

    /**
     * @return Chantier
     */
    public function getChantier ()
    {
        return $this->chantier;
    }

    /**
     * @param Chantier $chantier
     */
    public function setChantier ($chantier)
    {
        $this->chantier = $chantier;
    }



    public function __construct ()
    {
        $this->dateAcompt = new \DateTime();
    }
}
