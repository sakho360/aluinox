<?php
/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 11/23/17
 * Time: 12:51 PM
 */

namespace AppBundle\Events;


use AppBundle\Entity\Chantier;
use Symfony\Component\EventDispatcher\Event;

class ChantierEvent extends Event
{
    protected $chantier;

    public function __construct(Chantier $chantier)
    {
        $this->chantier = $chantier;
    }

    /**
     * @return Chantier
     */
    public function getChantier()
    {
        return $this->chantier;
    }

}