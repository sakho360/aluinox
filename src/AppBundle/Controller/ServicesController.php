<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Services;
use AppBundle\Form\ServicesType;
use function dump;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service controller.
 *
 * @Route("services")
 */
class ServicesController extends Controller
{
    /**
     * Lists all service entities.
     *
     * @Route("/", name="services_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $services = $em->getRepository('AppBundle:Services')->findAll();

        return $this->render('services/index.html.twig', array(
            'services' => $services,
        ));
    }

    /**
     * Creates a new service entity.
     *
     * @Route("/new", name="services_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $service = new Services();
        $form = $this->createForm(ServicesType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $service->setMontantRestant($service->getMontantRestant());
            //dump($service->getMontantRestant());die();
            if ($service->getMontantRestant() == 'invalid'){
               return $this->render('services/new.html.twig',[
                  'error' => "Le montant iniciale ne peut pas etre inferieur au montant donné à l'avance",
                   'form' => $form->createView(),
               ]);
            }
            else{
                $em->persist($service);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Le service '.$service->getNatureCeintrage().' a bien été creer !! ')
                ;
                return $this->redirect($this->generateUrl('services_index'));

                //return $this->redirectToRoute('services_show', array('id' => $service->getId()));
            }

        }

        return $this->render('services/new.html.twig', array(
            'service' => $service,
            'form' => $form->createView(),
            'error'=>false
        ));
    }

    /**
     * Finds and displays a service entity.
     *
     * @Route("/{id}", name="services_show")
     * @Method("GET")
     */
    public function showAction(Services $service)
    {
        $deleteForm = $this->createDeleteForm($service);

        return $this->render('services/show.html.twig', array(
            'service' => $service,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing service entity.
     *
     * @Route("/{id}/edit", name="services_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Services $service)
    {
        $deleteForm = $this->createDeleteForm($service);
        $editForm = $this->createForm('AppBundle\Form\ServicesType', $service);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('services_edit', array('id' => $service->getId()));
        }

        return $this->render('services/edit.html.twig', array(
            'service' => $service,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a service entity.
     *
     * @Route("/{id}", name="services_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Services $service)
    {
        $form = $this->createDeleteForm($service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($service);
            $em->flush();
        }

        return $this->redirectToRoute('services_index');
    }

    /**
     * Creates a form to delete a service entity.
     *
     * @param Services $service The service entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Services $service)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('services_delete', array('id' => $service->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
