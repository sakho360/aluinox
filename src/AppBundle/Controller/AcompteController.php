<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Acompte;
use AppBundle\Entity\Chantier;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Acompte controller.
 *
 * @Route("acompte")
 */
class AcompteController extends Controller
{
    /**
     * Lists all acompte entities.
     *
     * @Route("/", name="acompte_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $acomptes = $em->getRepository('AppBundle:Acompte')->findAll();

        return $this->render('acompte/index.html.twig', array(
            'acomptes' => $acomptes,
        ));
    }

    /**
     * Creates a new acompte entity.
     *
     * @Route("/new/{idChantier}", name="acompte_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Chantier $idChantier)
    {
        $acompte = new Acompte();
        $acompte->setChantier($idChantier);
        $form = $this->createForm('AppBundle\Form\AcompteType', $acompte);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($acompte);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Merci votre acompte a bien été creer !! ')
            ;

            return $this->redirect($this->generateUrl('chantier_show',array('id'=>$idChantier->getId())));

        }

        return $this->render('acompte/new.html.twig', array(
            'id'=>$acompte->getChantier()->getId(),
            'name'=>$acompte->getChantier()->getName(),
            'acompte' => $acompte,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a acompte entity.
     *
     * @Route("/{id}", name="acompte_show")
     * @Method("GET")
     */
    public function showAction(Acompte $acompte)
    {
        $deleteForm = $this->createDeleteForm($acompte);

        return $this->render('acompte/show.html.twig', array(
            'acompte' => $acompte,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing acompte entity.
     *
     * @Route("/{id}/edit", name="acompte_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Acompte $acompte)
    {
        $deleteForm = $this->createDeleteForm($acompte);
        $editForm = $this->createForm('AppBundle\Form\AcompteType', $acompte);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('acompte_edit', array('id' => $acompte->getId()));
        }

        return $this->render('acompte/edit.html.twig', array(
            'acompte' => $acompte,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a acompte entity.
     *
     * @Route("/{id}", name="acompte_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Acompte $acompte)
    {
        $form = $this->createDeleteForm($acompte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($acompte);
            $em->flush();
        }

        return $this->redirectToRoute('acompte_index');
    }

    /**
     * Creates a form to delete a acompte entity.
     *
     * @param Acompte $acompte The acompte entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Acompte $acompte)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('acompte_delete', array('id' => $acompte->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
